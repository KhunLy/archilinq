﻿using LINQDataContext;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqExercices
{
    class Program
    {
        static void Main(string[] args)
        {
            DataContext dc = new DataContext();

            //dc.Students
            //    .Where(e => e.BirthDate.Year < 1955)
            //    .Select(e => new
            //    {
            //        LN = e.Last_Name,
            //        Res = e.Year_Result,
            //        Status = e.Year_Result < 12 ? "KO" : "OK"
            //    })
            //    .ForEach(s => Console.WriteLine(
            //        $"{s.LN.ToUpper()} - {s.Res} - {s.Status}"
            //    ))
            //;

            //IEnumerable<IGrouping<int, Student>> ens 
            //    = dc.Students.GroupBy(s => s.Section_ID);
            //foreach (IGrouping<int, Student> item in ens)
            //{
            //    Console.WriteLine(item.Key);
            //    foreach (Student stu in item)
            //    {
            //        Console.WriteLine(stu.Last_Name);
            //    }
            //}

            //dc.Students.GroupBy(s => s.Section_ID)
            //    .ToList()
            //    .ForEach(g =>
            //    {
            //        Console.WriteLine(g.Key);
            //        g.ForEach(s => Console.WriteLine(s.Last_Name));
            //    });

            //dc.Students
            //    .Join(
            //        dc.Sections,
            //        (stu) => stu.Section_ID,
            //        (sec) => sec.Section_ID,
            //        (stu, sec) => new
            //        {
            //            stu.Last_Name,
            //            sec.Section_Name
            //        } 
            //)
            //.ForEach(j => 
            //    Console.WriteLine(j.Last_Name + j.Section_Name)
            //);

            dc.Sections.GroupJoin(
                dc.Students,
                se => se.Section_ID,
                st => st.Section_ID,
                (se, l) =>
                new {
                    key = se.Section_Name,
                    ListStudent = l.Select(s => new {
                        s.Last_Name,
                        s.First_Name
                    })
                }
            ).ToList()
            .ForEach(g => {
                Console.WriteLine(g.key);
                g.ListStudent
                    .ForEach(s => Console.WriteLine(s.Last_Name));
            });

            Console.ReadKey();
        }
    }

    static class MyLinq
    {
        public static IEnumerable<TOut> Select<TIn, TOut>(
            this IEnumerable<TIn> list,
            Func<TIn, TOut> selector
        )
        {
            foreach (TIn item in list)
            {
                yield return selector(item);
            }
        }

        public static void ForEach<T>(
            this IEnumerable<T> list,
            Action<T> function
        )
        {
            foreach (var item in list)
            {
                function(item);
            }
        }

        public static IEnumerable<T> Where<T>(
            this IEnumerable<T> list, Func<T,bool> predicate)
        {
            foreach (var item in list)
            {
                if (predicate(item))
                {
                    yield return item;
                }
            }
        }

        public static IEnumerable<TResult> Join<TL, TR, TKEY, TResult>(
            this IEnumerable<TL> left,
            IEnumerable<TR> right,
            Func<TL,TKEY> leftSelector,
            Func<TR, TKEY> rightSelector,
            Func<TL,TR,TResult> finalSelector
        )
        {
            foreach (TL leftItem in left)
            {
                foreach (TR rightItem in right)
                {
                    if (leftSelector(leftItem)
                        .Equals(rightSelector(rightItem)))
                    {
                        yield return finalSelector(leftItem, rightItem);
                    }
                }
            }
        }
    }
}
